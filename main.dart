import 'dart:io';

import 'A1.dart';
import 'Animal.dart';
import 'Game.dart';
import 'A2.dart';
import 'F1.dart';
import 'F2.dart';
import 'C1.dart';
import 'C2.dart';

void main() {
  Game g1 = Game(6, 0, '');
  print('กรอกชื่อของคุณ');
  g1.name = stdin.readLineSync();
  g1.showname();
  print('หมวดสัตว์ (ตอบเป็นภาษาอังกฤษ ตัวพิมพ์ใหญ่)');
  A1 a1 = A1(6, 0, 'เจ้าป่า');
  a1.showHpPointHint();
  g1.word = stdin.readLineSync();
  if (g1.word == 'LION') {
    g1.checkCor();
  } else {
    g1.checkIncor();
  }
  A2 a2 = A2(6, 100, 'ดุร้ายและตัวใหญ่');
  a2.showHpPointHint();
  g1.word = stdin.readLineSync();
  if (g1.word == 'BEAR') {
    g1.checkCor();
  } else {
    g1.checkIncor();
  }
  print('หมวดผลไม้ (ตอบเป็นภาษาอังกฤษ ตัวพิมพ์ใหญ่)');
  F1 f1 = F1(6, 200, 'เปลือกสีเหลือง');
  f1.showHpPointHint();
  g1.word = stdin.readLineSync();
  if (g1.word == 'BANANA') {
    g1.checkCor();
  } else {
    g1.checkIncor();
  }
  F2 f2 = F2(6, 300, 'มีลำต้นสูง นิยมดื่ม');
  f2.showHpPointHint();
  g1.word = stdin.readLineSync();
  if (g1.word == 'COCONUT') {
    g1.checkCor();
  } else {
    g1.checkIncor();
  }
  print('หมวดประเทศ (ตอบเป็นภาษาอังกฤษ ตัวพิมพ์ใหญ่)');
  C1 c1 = C1(6, 400, 'ต้มยำกุ้ง');
  c1.showHpPointHint();
  g1.word = stdin.readLineSync();
  if (g1.word == 'THAILAND') {
    g1.checkCor();
  } else {
    g1.checkIncor();
  }
  C2 c2 = C2(6, 500, 'สิงโตทะเล');
  c2.showHpPointHint();
  g1.word = stdin.readLineSync();
  if (g1.word == 'SINGAPORE') {
    g1.checkCor();
    print('เก่งมาก คะแนนของคุณคือ 500');
  } else {
    g1.checkIncor();
  }
}
